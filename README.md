# Weather App

This is a simple weather app to showcase current weather data of your city, it shows forecast for 5 days with 3 hours interval.  
It has a search by city name functionality with searching for 7b cities supported.

## Build Instructions



1. Checkout the master branch. 
    

2. Open android studio, file->new -> import project and select app folder.

3. Wait for sync and run it.


##   Testing

1. Checkout the app in android studio. 

2. On test folder right click-> run tests with coverage, covergae can be seen there.

3. On androidTestFolder run the test cases .