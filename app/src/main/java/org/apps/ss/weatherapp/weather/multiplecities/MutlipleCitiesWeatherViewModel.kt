package org.apps.ss.weatherapp.weather.multiplecities

import androidx.lifecycle.ViewModel;
import org.apps.ss.weatherapp.model.repository.WeatherRepository
import javax.inject.Inject

class MutlipleCitiesWeatherViewModel @Inject constructor(val weatherRepository: WeatherRepository): ViewModel() {

    fun getCity(city :String ) = weatherRepository.getCity(city)
}
