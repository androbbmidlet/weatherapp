package org.apps.ss.weatherapp.weather.multiplecities

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.apps.ss.weatherapp.R
import org.apps.ss.weatherapp.model.WeatherCityResponse
import org.apps.ss.weatherapp.network.AppConfig

class MutilpleCityAdaptor (private val weatherCityResponseList : List<WeatherCityResponse?>?)
    : RecyclerView.Adapter<MutilpleCityAdaptor.CityViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.weather_detail_row, parent,
            false))
    }


    override fun getItemCount(): Int {
        if(weatherCityResponseList!=null) {
            return weatherCityResponseList.size
        }
        return 0
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        val weatherCityResponse = weatherCityResponseList?.get(position)
        weatherCityResponse?.let {
            holder.tvCityName.text = weatherCityResponse.name
            holder.tvTempMinVal.text = String.format(holder.tvTempMinVal.text.toString(),weatherCityResponse.main?.temp_min)
            holder.tvTempMaxVal.text = String.format(holder.tvTempMaxVal.text.toString(),weatherCityResponse.main?.temp_max)
            if( weatherCityResponse.weather.size>0) {
                holder.tvWeatherVal.text = weatherCityResponse.weather.get(0).description
            }
            holder.tvWindVal.text =
            """${weatherCityResponse.wind?.speed} ${AppConfig.meter_per_sec}"""



        }
    }


    class CityViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        val tvTempMinVal = view.findViewById<TextView>(R.id.tempratureMinVal)
        val tvTempMaxVal = view.findViewById<TextView>(R.id.tempratureMaxVal)
        val tvWeatherVal = view.findViewById<TextView>(R.id.weatherDescVal)
        val tvWindVal = view.findViewById<TextView>(R.id.windVal)
        val tvCityName = view.findViewById<TextView>(R.id.cityName)


    }
}