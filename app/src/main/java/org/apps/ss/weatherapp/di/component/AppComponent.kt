package org.apps.ss.weatherapp.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import org.apps.ss.weatherapp.WeatherApplication
import org.apps.ss.weatherapp.di.module.ActivityModule
import org.apps.ss.weatherapp.di.module.AppModule
import org.apps.ss.weatherapp.di.module.MainActivityModule
import org.apps.ss.weatherapp.weather.currentcity.WeatherCurrentCityViewModel
import org.apps.ss.weatherapp.weather.multiplecities.MutlipleCitiesWeatherViewModel
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        MainActivityModule::class,
        AppModule::class

    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: WeatherApplication)



    fun inject(weatherCurrentCityViewModel: WeatherCurrentCityViewModel)


    fun inject(mutlipleCitiesWeatherViewModel: MutlipleCitiesWeatherViewModel)



}