package org.apps.ss.weatherapp.weather.currentcity

import androidx.lifecycle.ViewModel;
import org.apps.ss.weatherapp.model.repository.WeatherRepository
import javax.inject.Inject

class WeatherCurrentCityViewModel @Inject constructor(val weatherRepository: WeatherRepository): ViewModel() {

    fun getCurrent(lat: String, lon :String )  = weatherRepository.getCurrent(lat,lon)


}
