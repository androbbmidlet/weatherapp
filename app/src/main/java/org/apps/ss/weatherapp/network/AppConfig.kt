package org.apps.ss.weatherapp.network

class AppConfig {
    companion object{

        val KELVIN_CONST =273.15
        val max_city: Int = 7
        val min_city: Int = 3
        val meter_per_sec: String = "m/s"

        val API_URL ="https://api.openweathermap.org/data/2.5/"

    }

}