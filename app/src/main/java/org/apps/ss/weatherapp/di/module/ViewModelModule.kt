package org.apps.ss.weatherapp.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import org.apps.ss.weatherapp.di.key.ViewModelKey
import org.apps.ss.weatherapp.weather.currentcity.WeatherCurrentCityViewModel
import org.apps.ss.weatherapp.weather.multiplecities.MutlipleCitiesWeatherViewModel

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(WeatherCurrentCityViewModel::class)
    abstract fun bindWeatherCurrentCityViewModel(viewModel: WeatherCurrentCityViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(MutlipleCitiesWeatherViewModel::class)
    abstract fun bindMutlipleCitiesWeatherViewModel(viewModel: MutlipleCitiesWeatherViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}