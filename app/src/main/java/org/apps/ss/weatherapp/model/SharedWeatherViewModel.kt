package org.apps.ss.weatherapp.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedWeatherViewModel : ViewModel() {
    val weatherCityResponseList = MutableLiveData<List<WeatherCityResponse>>()

}