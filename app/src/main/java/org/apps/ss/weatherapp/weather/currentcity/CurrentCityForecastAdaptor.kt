package org.apps.ss.weatherapp.weather.currentcity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.apps.ss.weatherapp.R
import org.apps.ss.weatherapp.model.WeatherForeCastModel
import org.apps.ss.weatherapp.util.LineDividerItemDecoration
import org.apps.ss.weatherapp.util.Utils

class CurrentCityForecastAdaptor(
    private val weatherForeCastModelMap: Map<String?, List<WeatherForeCastModel?>>
) : RecyclerView.Adapter<CurrentCityForecastAdaptor.CityViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.weather_forecast_detail_row, parent,
                false
            )
        )
    }


    override fun getItemCount() = weatherForeCastModelMap.size


    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {

        val weatherForeCastModel = weatherForeCastModelMap.keys.toList()
        val dateS = weatherForeCastModel.get(position)
        weatherForeCastModelMap.get(dateS)?.let { w ->
            if (w.isNotEmpty()) {
                val date = w.get(0)?.dt_txt
                date?.let {
                    val dateNow = it.indexOf(" ").let { it1 -> it.substring(0, it1) }
                    holder.tvTodayDate.text = dateNow

                }

            }

            val currentCityDayForecastAdaptor = CurrentCityDayForecastAdaptor(w)
            holder.recyclerViewCurrentDate.context?.let {
                val divider = Utils.getDrawable(it, R.drawable.divider_line)
                divider?.let {
                    val itemDecor = LineDividerItemDecoration(it)
                    holder.recyclerViewCurrentDate.addItemDecoration(itemDecor)

                }

            }
            holder.recyclerViewCurrentDate.adapter = currentCityDayForecastAdaptor
            holder.recyclerViewCurrentDate.layoutManager =
                LinearLayoutManager(holder.recyclerViewCurrentDate.context, RecyclerView.VERTICAL, false)

        }
    }


    class CityViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        var recyclerViewCurrentDate: RecyclerView = view.findViewById(R.id.weatherCurrentDateRecyclerView)
        val tvTodayDate = view.findViewById<TextView>(R.id.dateLabel)


    }
}