package org.apps.ss.weatherapp.weather.currentcity

import android.content.pm.PackageManager
import android.location.Location
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

import org.apps.ss.weatherapp.R
import org.apps.ss.weatherapp.base.BaseFragment
import org.apps.ss.weatherapp.di.injector.Injectable
import org.apps.ss.weatherapp.model.WeatherForeCastModel
import org.apps.ss.weatherapp.model.WeatherForeCastResponse
import org.apps.ss.weatherapp.network.AppConfig
import org.apps.ss.weatherapp.weather.LocationUtil
import javax.inject.Inject
import kotlin.math.roundToInt

class WeatherCurrentCityFragment @Inject constructor() : BaseFragment(), Injectable {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: WeatherCurrentCityViewModel

    private var progressBar: ProgressBar? = null

    private var weatherCurrentRecyclerView: RecyclerView? = null

    private var cityNameView: TextView? = null


    private var compositeDisposable = CompositeDisposable()
    private var weatherForeCastResponseList: List<WeatherForeCastModel?>? = null
    private var weatherForeCastResponse: WeatherForeCastResponse? = null
    private var currentCityForecastAdaptor: CurrentCityForecastAdaptor? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    override fun getLayoutRes(): Int = R.layout.weather_current_city_fragment

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(WeatherCurrentCityViewModel::class.java)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressBar = view.findViewById(R.id.progressBar)
        cityNameView = view.findViewById(R.id.cityName)
        weatherCurrentRecyclerView = view.findViewById(R.id.weatherCurrentRecyclerView)

        initLocation(view, LocationUtil.isLocationEnabled(context))
        checkInternet(view)

    }

    /**
     * initializes location related modules
     */
    private fun initLocation(view: View, locationEnabled: Boolean) {
        if (checkLocationPermissions()) {
            context?.let {

                if (locationEnabled) {
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(it)
                    mFusedLocationClient?.let {
                        LocationUtil.getLastLocation(this, it, mLocationCallback)

                    }
                } else {
                    showError(view, getString(R.string.enable_location_settings))
                }
            }
        } else {
            showError(view, getString(R.string.location_permissions))

            LocationUtil.requestPermissions(this)

        }


    }

    /**
     * checks if user is connected to internet
     */
    private fun checkInternet(view: View) {

        if (!LocationUtil.checkInternet(context)) {
            showError(view, getString(R.string.check_internet))
        }
    }


    private fun checkLocationPermissions(): Boolean {
        return LocationUtil.checkPermissions(this)
    }


    private fun progress(booleanHide: Boolean) {
        if (booleanHide) {
            progressBar?.visibility = View.GONE

        } else {
            progressBar?.visibility = View.VISIBLE

        }

    }

    private fun showError(view: View, message: String) {
        showSnackBar(view, message)
        progress(true)
    }


    /**
     * location call back for location updates
     */
    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            progress(true)
            getCurrentLocationWeatherData(locationResult.lastLocation)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    /**
     * callback for when user has given or rejected permission
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == LocationUtil.PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

                if (LocationUtil.isLocationEnabled(context)) {
                    mFusedLocationClient = activity?.let { LocationServices.getFusedLocationProviderClient(it) }
                    mFusedLocationClient?.let {
                        LocationUtil.getLastLocation(WeatherCurrentCityFragment@ this, it, mLocationCallback)

                    }
                } else {
                    activity?.findViewById<View>(R.id.container)?.let {
                        showError(it, getString(R.string.enable_location_settings))
                    }

                }
            } else {

                activity?.findViewById<View>(R.id.container)?.let {
                    progress(true)
                    showSnackBarWithAction(it, getString(R.string.location_permissions),
                        getString(R.string.retry), object : View.OnClickListener {
                            override fun onClick(view: View?) {
                                LocationUtil.requestPermissions(this@WeatherCurrentCityFragment)
                            }

                        })

                }


            }

        }

    }

    /**
     * get the current city data based on location and display it in details fragment
     */
    private fun getCurrentLocationWeatherData(locationCurrent: Location?) {

        locationCurrent?.let { location ->

            val disposable: Disposable =
                viewModel.getCurrent(location.latitude.toString(), location.longitude.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe() {
                        progress(false)
                    }
                    .doFinally {
                        progress(true)
                        showCurrentForecastWeather()
                    }
                    .subscribe({ w ->
                        run {
                            weatherForeCastResponse = w
                            cityNameView?.text =
                                String.format(getString(R.string.five_day_forcast), weatherForeCastResponse?.city?.name)
                            weatherForeCastResponseList = w?.list

                        }
                    }) { e ->
                        run {
                            progress(true)
                            activity?.findViewById<View>(R.id.container)
                                ?.let { it1 -> showError(it1, getString(R.string.error_loading)) }

                        }
                    }
            compositeDisposable.add(disposable)
        }
    }


    private fun showCurrentForecastWeather() {

        weatherForeCastResponseList?.let {
            weatherForeCastResponseList
            val result = it.groupBy {
                it?.dt_txt?.let {
                    it.substring(0, it.indexOf(" "))
                }
            }
            if (result.isNotEmpty()) {

                result.values.forEach { resultInner ->
                    resultInner.forEach {
                        it?.main?.temp_max =
                            (it?.main?.temp_max?.toDouble()?.minus(AppConfig.KELVIN_CONST))?.roundToInt().toString()
                        it?.main?.temp_min =
                            (it?.main?.temp_min?.toDouble()?.minus(AppConfig.KELVIN_CONST))?.roundToInt().toString()

                    }
                }

                currentCityForecastAdaptor = CurrentCityForecastAdaptor(result)
                weatherCurrentRecyclerView?.adapter = currentCityForecastAdaptor




                weatherCurrentRecyclerView?.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)


            } else {
                activity?.findViewById<View>(R.id.container)?.let {
                    showSnackBar(
                        it, getString(
                            R.string.error_data_load
                        )
                    )
                }
            }

        }
    }

}
