package org.apps.ss.weatherapp.model

class WeatherForeCastModel {

    var main: WeatherMain? = null

    var weather = mutableListOf<Weather>()

    var wind:Wind? = null

    var dt_txt:String? = null

}