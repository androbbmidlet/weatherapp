package org.apps.ss.weatherapp.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.apps.ss.weatherapp.MainActivity
import org.apps.ss.weatherapp.di.module.FragmentModule


@Suppress("unused")
@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeMainActivity(): MainActivity
}