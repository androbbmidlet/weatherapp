package org.apps.ss.weatherapp.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.apps.ss.weatherapp.weather.currentcity.WeatherCurrentCityFragment
import org.apps.ss.weatherapp.weather.multiplecities.MultlipleCitiesWeatherFragment

@Suppress("unused")
@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): WeatherCurrentCityFragment


    @ContributesAndroidInjector
    abstract fun contributeMutlipleCitiesWeatherFragment(): MultlipleCitiesWeatherFragment

}