package org.apps.ss.weatherapp.di.module

import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import dagger.Binds
import org.apps.ss.weatherapp.MainActivity


@Module(subcomponents = [MainActivityActivitySubcomponent::class])
abstract class ActivityModule {

    @Binds
    @IntoMap
    @ClassKey(MainActivity::class)
    abstract fun bindMainAndroidInjectorFactory(factory: MainActivityActivitySubcomponent.Factory): AndroidInjector.Factory<*>




}

@Subcomponent(modules = [FragmentModule::class])
interface MainActivityActivitySubcomponent: AndroidInjector<MainActivity> {
    @Subcomponent.Factory
    interface Factory: AndroidInjector.Factory<MainActivity> {}
}

