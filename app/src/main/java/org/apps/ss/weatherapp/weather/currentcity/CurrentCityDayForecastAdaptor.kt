package org.apps.ss.weatherapp.weather.currentcity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.apps.ss.weatherapp.R
import org.apps.ss.weatherapp.model.WeatherForeCastModel
import org.apps.ss.weatherapp.network.AppConfig

class CurrentCityDayForecastAdaptor (private val weatherForeCastModelList : List<WeatherForeCastModel?>?)
    : RecyclerView.Adapter<CurrentCityDayForecastAdaptor.CityViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.weather_forecast_current_day_row, parent,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        if(weatherForeCastModelList!=null) {
            return weatherForeCastModelList.size
        }

        return 0
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        val weatherForeCastModel = weatherForeCastModelList?.get(position)
        weatherForeCastModel?.let {
            holder.tvTempVal.text = String.format(holder.tvTempVal.text.toString(),it.main?.temp_min,it.main?.temp_max)

            it.dt_txt?.let {
                val time = it.substring(it.indexOf(" ")+1)
                holder.tvTimeLabel.text = time

            }

            if( weatherForeCastModel.weather.size>0) {
                holder.tvWeatherDescVal.text = weatherForeCastModel.weather.get(0).description
            }
            holder.tvWindVal.text =
                """${weatherForeCastModel.wind?.speed} ${AppConfig.meter_per_sec}"""



        }
    }


    class CityViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        val tvTimeLabel = view.findViewById<TextView>(R.id.timeLabel)
        val tvTempVal = view.findViewById<TextView>(R.id.tempVal)
        val tvWindVal = view.findViewById<TextView>(R.id.windVal)
        val tvWeatherDescVal = view.findViewById<TextView>(R.id.weatherDescVal)




    }
}