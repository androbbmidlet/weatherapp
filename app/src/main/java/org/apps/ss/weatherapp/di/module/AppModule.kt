package org.apps.ss.weatherapp.di.module

import dagger.Module

@Module(includes = [ViewModelModule::class, NetworkModule::class])
class AppModule {


}