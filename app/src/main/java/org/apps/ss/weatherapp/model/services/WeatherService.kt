package org.apps.ss.weatherapp.model.services

import io.reactivex.Single
import org.apps.ss.weatherapp.model.WeatherCityResponse
import org.apps.ss.weatherapp.model.WeatherForeCastModel
import org.apps.ss.weatherapp.model.WeatherForeCastResponse
import retrofit2.http.*

interface WeatherService {


    //api.openweathermap.org/data/2.5/forecast?lat=35&lon=139

    //api.openweathermap.org/data/2.5/weather?q=London

    @POST("forecast?appid=39c6bbb908910d3a34bbd7df2d8d232d")
    fun getCurrent(@Query("lat")  lat: String, @Query("lon") lon :String) : Single<WeatherForeCastResponse>


    @POST("weather?appid=39c6bbb908910d3a34bbd7df2d8d232d")
    fun getCity(@Query("q")  city: String) : Single<WeatherCityResponse>

}