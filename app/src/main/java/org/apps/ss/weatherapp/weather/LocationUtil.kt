package org.apps.ss.weatherapp.weather

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Looper
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.*

class LocationUtil {

    companion object {


        const val PERMISSION_ID: Int = 100

        fun checkPermissions(fragment: Fragment?): Boolean {

            fragment?.context?.let {


                if (ActivityCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                    && (ActivityCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED)
                ) {
                    return true
                }
            }
            return false
        }

        fun requestPermissions(fragment: Fragment?) {
            fragment?.let {
                it.requestPermissions(

                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSION_ID
                )

            }
        }

        fun isLocationEnabled(context: Context?): Boolean {


            context?.let {

                val locationManager: LocationManager = it.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                    LocationManager.NETWORK_PROVIDER
                )
            }

            return false

        }


        @SuppressLint("MissingPermission")
        fun getLastLocation(
            fragment: Fragment?, mFusedLocationClient: FusedLocationProviderClient,
            locationCallback: LocationCallback
        ) {


            fragment?.context?.let { context ->


                if (checkPermissions(fragment)) {
                    if (isLocationEnabled(context)) {

                        fragment.activity?.let {
                            mFusedLocationClient.lastLocation.addOnCompleteListener(it) { task ->
                                val location: Location? = task.result
                                if (location == null) {
                                    requestNewLocationData(fragment, mFusedLocationClient, locationCallback)
                                } else {

                                    locationCallback.onLocationResult(LocationResult.create(mutableListOf(location)))

                                }
                            }
                        }
                    }
                } else {
                    requestPermissions(fragment)

                }
            }
        }

        @SuppressLint("MissingPermission")
        fun requestNewLocationData(
            fragment: Fragment?,
            mFusedLocationClient: FusedLocationProviderClient,
            locationCallback: LocationCallback) {
            val mLocationRequest = LocationRequest()
            mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            mLocationRequest.interval = 1
            mLocationRequest.fastestInterval = 1
            mLocationRequest.numUpdates = 1
            fragment?.let {
                mFusedLocationClient.requestLocationUpdates(
                    mLocationRequest, locationCallback,
                    Looper.getMainLooper()
                )
            }
        }

        fun checkInternet(context: Context?): Boolean {

            var isConnected = false
            context?.let {
                val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
                isConnected = activeNetwork?.isConnectedOrConnecting == true

            }

            return isConnected

        }

    }


}