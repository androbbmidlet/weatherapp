package org.apps.ss.weatherapp.base

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment : Fragment() {

abstract fun getLayoutRes():Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(getLayoutRes(),null)
    }

    fun showSnackBar(view: View, text:String){
        val snackbar = Snackbar.make(view, text,
            Snackbar.LENGTH_LONG)

        snackbar.show()
    }

    fun showSnackBarWithAction(view: View, text:String, actionText:String,onClickListener: View.OnClickListener){
        val snackbar = Snackbar.make(view, text,
            Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(actionText,onClickListener)

        snackbar.show()
    }


}