package org.apps.ss.weatherapp.weather.multiplecities

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.lifecycle.ViewModelProvider
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import org.apps.ss.weatherapp.R
import org.apps.ss.weatherapp.base.BaseFragment
import org.apps.ss.weatherapp.di.injector.Injectable
import org.apps.ss.weatherapp.model.SharedWeatherViewModel
import org.apps.ss.weatherapp.model.WeatherCityResponse
import org.apps.ss.weatherapp.network.AppConfig
import org.apps.ss.weatherapp.weather.LocationUtil
import org.apps.ss.weatherapp.weather.LocationUtil.Companion.checkInternet
import java.lang.Exception
import javax.inject.Inject
import kotlin.math.roundToInt

/**
 * Class to provide UI for multiple cities weather data, using android fragment
 */
class MultlipleCitiesWeatherFragment @Inject constructor() : BaseFragment(), Injectable {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MutlipleCitiesWeatherViewModel
    private var progressBar: ProgressBar? = null

    private var compositeDisposable = CompositeDisposable()
    private var sharedWeatherViewModel: SharedWeatherViewModel? = null




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.mutliple_cities_weather_fragment, container, false)
    }

    override fun getLayoutRes(): Int = R.layout.mutliple_cities_weather_fragment


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val editTextCities = view.findViewById<EditText>(R.id.citiesEditText)
        val buttonSearch = view.findViewById<Button>(R.id.searchButton)
        progressBar = view.findViewById(R.id.progressBar)

        buttonSearch.setOnClickListener() {
            val text = editTextCities.text.toString()

            if (text.isEmpty()) {

                showSnackBar(view, getString(R.string.enter_city_name))


            } else if (text.indexOf(",") == -1 && text.isEmpty()) {
                showSnackBar(view, getString(R.string.comma_seperated_cities))

            } else {

                val cities = text.split(",").toTypedArray()
                if (cities.size < AppConfig.min_city || cities.size > AppConfig.max_city) {
                    showSnackBar(view, getString(R.string.city_limit))
                } else {

                    getCitiesWeather(cities)
                }
            }


        }

        checkInternet(view)

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MutlipleCitiesWeatherViewModel::class.java)

        sharedWeatherViewModel = activity?.let {
            ViewModelProviders.of(it)
        }?.get(SharedWeatherViewModel::class.java)


    }

    private fun getCityWeatherObservable(city: String): Observable<WeatherCityResponse> {
        return viewModel.getCity(city)
            .subscribeOn(Schedulers.io())
            .toObservable()

    }


    private fun getCitiesWeather(cities: Array<String>) {


        val list = mutableListOf<Observable<WeatherCityResponse>>()
        cities.forEach { it.trim() }
        cities.forEach {

            val observable = getCityWeatherObservable(it)
            list.add(observable)

        }

        val weatherResponses = mutableListOf<WeatherCityResponse>()

        val disposable = Observable.concatDelayError(list)
            .doOnSubscribe {
                progress(false)
            }
            .map {

                try {
                    weatherResponses.add(it)
                    it
                } catch (e: Exception) {
                    it
                }

            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { error ->
                error.localizedMessage

            }

            .doFinally {

                progress(true)
                if (weatherResponses.size > 0) {
                    if (weatherResponses.size != cities.size) {
                        activity?.findViewById<View>(R.id.container)?.let { it1 ->
                            showError(it1, getString(R.string.some_cities_not_loaded))
                        }

                    }

                    weatherResponses.forEach {

                        it.main?.temp_max =
                            (it.main?.temp_max?.toDouble()?.minus(AppConfig.KELVIN_CONST))?.roundToInt()
                                .toString()
                        it.main?.temp_min =
                            (it.main?.temp_min?.toDouble()?.minus(AppConfig.KELVIN_CONST))?.roundToInt()
                                .toString()


                    }

                    activity?.supportFragmentManager?.let {
                        it.beginTransaction()
                            .replace(R.id.container, WeatherDetailFragment.newInstance())
                            .addToBackStack(WeatherDetailFragment.toString())
                            .commit()

                        sharedWeatherViewModel?.weatherCityResponseList?.value = (weatherResponses)

                    }
                } else {
                    activity?.findViewById<View>(R.id.container)
                        ?.let { it1 -> showError(it1, getString(R.string.no_cities_loades)) }

                }


            }
            .subscribe({

            }, {
                activity?.findViewById<View>(R.id.container)
                    ?.let { it1 -> showError(it1, getString(R.string.error_loading)) }

            })

        compositeDisposable.add(disposable)


    }

    private fun checkInternet(view: View) {


        if (!checkInternet(context)) {


            showError(view, getString(R.string.check_internet))


        }
    }

    private fun showError(view: View, message: String) {
        showSnackBar(view, message)
        progress(true)
    }

    private fun progress(booleanHide: Boolean) {
        if (booleanHide) {
            progressBar?.visibility = View.GONE

        } else {
            progressBar?.visibility = View.VISIBLE

        }

    }

}
