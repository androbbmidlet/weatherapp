package org.apps.ss.weatherapp.util

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat

object Utils {

    fun getDrawable(context: Context, id: Int): Drawable? {
        return ContextCompat.getDrawable(context, id)
    }
}