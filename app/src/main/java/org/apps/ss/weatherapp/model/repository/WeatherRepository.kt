package org.apps.ss.weatherapp.model.repository

import org.apps.ss.weatherapp.model.services.WeatherService
import javax.inject.Inject

class WeatherRepository  @Inject constructor( private val weatherService: WeatherService){

    fun getCurrent(lat: String, lon :String ) = weatherService.getCurrent(lat,lon)

    fun getCity(city :String ) = weatherService.getCity(city)

}