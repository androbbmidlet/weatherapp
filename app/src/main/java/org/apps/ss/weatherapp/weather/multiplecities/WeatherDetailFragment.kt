package org.apps.ss.weatherapp.weather.multiplecities

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import org.apps.ss.weatherapp.R
import org.apps.ss.weatherapp.model.SharedWeatherViewModel
import org.apps.ss.weatherapp.model.WeatherCityResponse

class WeatherDetailFragment : Fragment() {

    companion object {
        fun newInstance() = WeatherDetailFragment()
    }
    private  var sharedWeatherViewModel: SharedWeatherViewModel? = null
    private var weatherCityResponseList = listOf<WeatherCityResponse>()
    private var weatherCitiesRecyclerView : RecyclerView? =null
    var mutilpleCityAdaptor : MutilpleCityAdaptor? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.weather_detail_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedWeatherViewModel = activity?.let {
            ViewModelProviders.of(it)
        }?.get(SharedWeatherViewModel::class.java)
        sharedWeatherViewModel?.weatherCityResponseList?.observe(viewLifecycleOwner,
            Observer <List<WeatherCityResponse>>{
                weatherCityResponseList = it
                mutilpleCityAdaptor = MutilpleCityAdaptor(weatherCityResponseList)
                weatherCitiesRecyclerView?.adapter = mutilpleCityAdaptor
                weatherCitiesRecyclerView?.layoutManager = LinearLayoutManager(context,RecyclerView.VERTICAL,false)

        })


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        weatherCitiesRecyclerView = view.findViewById(R.id.weatherCitiesRecyclerView)




    }

}
