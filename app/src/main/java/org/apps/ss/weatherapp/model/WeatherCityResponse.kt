package org.apps.ss.weatherapp.model

class WeatherCityResponse{

    var main: WeatherMain? = null
    var wind: Wind? =null
    var name : String? = null
    var weather = arrayOf<Weather>()

}