package org.apps.ss.weatherapp.di.injector

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable


interface UnInjectable