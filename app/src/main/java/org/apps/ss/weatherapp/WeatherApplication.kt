package org.apps.ss.weatherapp

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import org.apps.ss.weatherapp.di.component.AppComponent
import org.apps.ss.weatherapp.di.injector.AppInjector
import javax.inject.Inject

class WeatherApplication :Application(), LifecycleObserver, HasAndroidInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector() = dispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

    }


    private fun initializeDagger() {
        appComponent = AppInjector.init(this)
    }


    companion object {

        lateinit var appComponent: AppComponent
        var isForeground = true

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        //App in background
        isForeground = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        // App in foreground
        isForeground = true
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }




}