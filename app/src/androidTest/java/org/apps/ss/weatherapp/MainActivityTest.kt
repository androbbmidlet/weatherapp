package org.apps.ss.weatherapp

import android.content.Context
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.InstrumentationRegistry
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.contrib.NavigationViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import org.apps.ss.weatherapp.weather.multiplecities.MultlipleCitiesWeatherFragment
import org.hamcrest.core.AllOf.*
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@LargeTest
class MainActivityTest{
    @get:Rule
    var activityRule: ActivityTestRule<MainActivity>
            = ActivityTestRule(MainActivity::class.java)


    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = ApplicationProvider.getApplicationContext<Context>()
        assertEquals("org.apps.ss.weatherapp", appContext.packageName)
    }



    @Test
    fun testCurrentForecastView() {


        Thread.sleep(1000)

        // Check if the add task screen is displayed
        onView(withId(R.id.citiesEditText)).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.searchButton), isDisplayed()))

    }





}