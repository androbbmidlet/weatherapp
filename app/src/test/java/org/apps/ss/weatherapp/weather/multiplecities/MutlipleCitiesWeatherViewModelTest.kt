package org.apps.ss.weatherapp.weather.multiplecities

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Observable
import io.reactivex.Single
import org.apps.ss.weatherapp.model.WeatherCityResponse
import org.apps.ss.weatherapp.model.WeatherForeCastResponse
import org.apps.ss.weatherapp.model.repository.WeatherRepository
import org.apps.ss.weatherapp.weather.currentcity.WeatherCurrentCityViewModel
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class MutlipleCitiesWeatherViewModelTest{
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val repository = Mockito.mock(WeatherRepository::class.java)
    private val context = Mockito.mock(Application::class.java)
    private val viewModel = MutlipleCitiesWeatherViewModel(repository)

    @Test
    fun testGetCity() {
        //  val foo = Single<WeatherCityResponse>()
        var obser = Observable.just(WeatherCityResponse())
        var single = Single.fromObservable(obser)



        Mockito.`when`(repository.getCity("Kuwait")).thenReturn(single)

        viewModel.getCity("Kuwait")

        Mockito.verify(repository).getCity("Kuwait")

        Mockito.verifyNoMoreInteractions(repository)
    }
}