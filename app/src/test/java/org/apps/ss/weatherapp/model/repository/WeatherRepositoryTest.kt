package org.apps.ss.weatherapp.model.repository

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Single
import io.reactivex.functions.Consumer
import org.apps.ss.weatherapp.TestUtil
import org.apps.ss.weatherapp.model.WeatherCityResponse
import org.apps.ss.weatherapp.model.services.WeatherService
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito

class WeatherRepositoryTest {


    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()
    private val weatherService = Mockito.mock(WeatherService::class.java)


    private val repository = WeatherRepository(weatherService)
    private val context = Mockito.mock(Application::class.java)

    @Test
    fun getCurrent() {

        val response = TestUtil.getCurrentCityForecast
        Mockito.`when`(weatherService.getCurrent(ArgumentMatchers.anyString(),ArgumentMatchers.anyString())).
            thenReturn(Single.just(response))

        repository.getCurrent("1","2").subscribe { s->
            run {
                Assert.assertEquals(s,response)

            }
        }
        Mockito.verify(weatherService).getCurrent(ArgumentMatchers.anyString()
            ,ArgumentMatchers.anyString())
        Mockito.verifyNoMoreInteractions(weatherService)
    }

    @Test
    fun getCity() {

        val response = TestUtil.getCityData
        Mockito.`when`(weatherService.getCity(ArgumentMatchers.anyString())).
            thenReturn(Single.just(response))

        repository.getCity("Noida").subscribe { s->
            run {
                Assert.assertEquals(s,response)

            }
        }
        Mockito.verify(weatherService).getCity(ArgumentMatchers.anyString())
        Mockito.verifyNoMoreInteractions(weatherService)

    }
}