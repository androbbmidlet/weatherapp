package org.apps.ss.weatherapp

import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import org.apps.ss.weatherapp.model.WeatherCityResponse
import org.apps.ss.weatherapp.model.WeatherForeCastResponse
import java.io.InputStreamReader

object TestUtil {
    val getCityData: WeatherCityResponse by lazy {
        val inputStream = javaClass.classLoader
            ?.getResourceAsStream("mock/get_city_weather_data.json")

        val parser = JsonParser().parse(InputStreamReader(inputStream))

        val type = object : TypeToken<WeatherCityResponse>() {}.type
        val gson = Gson().fromJson<WeatherCityResponse>(parser, type)
        gson
    }

    val getCurrentCityForecast: WeatherForeCastResponse by lazy {
        val inputStream = javaClass.classLoader
            ?.getResourceAsStream("mock/get_forecast_5_current.json")

        val parser = JsonParser().parse(InputStreamReader(inputStream))

        val type = object : TypeToken<WeatherForeCastResponse>() {}.type
        val gson = Gson().fromJson<WeatherForeCastResponse>(parser, type)
        gson
    }
}