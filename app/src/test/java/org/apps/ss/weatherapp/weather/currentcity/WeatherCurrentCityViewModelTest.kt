package org.apps.ss.weatherapp.weather.currentcity

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.Single
import org.apps.ss.weatherapp.TestUtil
import org.apps.ss.weatherapp.model.WeatherCityResponse
import org.apps.ss.weatherapp.model.WeatherForeCastResponse
import org.apps.ss.weatherapp.model.repository.WeatherRepository
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito


class WeatherCurrentCityViewModelTest{
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val repository = Mockito.mock(WeatherRepository::class.java)
    private val viewModel = WeatherCurrentCityViewModel(repository)

    @Test
    fun testGetCurrent() {
      //  val foo = Single<WeatherCityResponse>()
        var obser = Observable.just(WeatherForeCastResponse())
        var single = Single.fromObservable(obser)

        Mockito.`when`(repository.getCurrent("12","34")).thenReturn(single)

        viewModel.getCurrent("12","34")

        Mockito.verify(repository).getCurrent("12","34")

        Mockito.verifyNoMoreInteractions(repository)
    }

}